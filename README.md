This is my own little pack of managing utilites, 
to do typical administrative tasks on VDS servers.

- disk-findinodes.sh — finding dirs witn many files and list them in sort

- malware-rigelbatch.sh — finding poplular website/PHP malware with log
- malware-rigelmail.sh — finding malware and send email reports

- mysql-autodump.sh — regular auto mysql dumps with rotate old dumps

- postfix-accessedit.sh — just to edit `/etc/postfix/access`, postmap it and reload postfix
- postfix-cleanq.sh — cleaning postfix mailqueue
- postfix-readq.sh — reading messages from postfix mailqueue winth comfort
- postfix-testmail.sh — sending simple test mail with system MTA
- postfix-undeliver-report.sh - generating and sending mail reports about undelivered e-mails
- postfix-virtualedit.sh — just edit `/etc/postfix/virtual`, postmap it and reload postfix

- mail-sendtest.sh — just a simple script, who sending a little test mail on some address 

- monitoring-esm.sh — automatic deploying and execute ESM'sh from [ezservermonitor.com](http://ezservermonitor.com/)
- monitoring-esmweb-inst.sh — automatic deploying ESM'web dashboard from [ezservermonitor.com](http://ezservermonitor.com/) to the custom folder
