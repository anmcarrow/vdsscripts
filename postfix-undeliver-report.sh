#!/bin/bash

ADMINMAIL="aleksandr.ermakov@me.com"

TARGETMAILS="baron@karlmunchausen.ru postmaster@karlmunchausen.ru abuse@karlmunchausen.ru mailbox@studio101.ru chronicler@studio101.ru svartalfr@studio101.ru cruach@studio101.ru deadlands@studio101.ru kota@studio101.ru postmaster@studio101.ru abuse@studio101.ru"
#TARGETMAILS="mailbox@studio101.ru"

LANG=C

for CURRENTMAIL in $TARGETMAILS ; do

echo -e "Rejected senders for ${CURRENTMAIL} at $(date):"
grep "to=<${CURRENTMAIL}" $ML | grep --color 'rejected' | \
cut -d ' ' -f20 | \
grep -v 'to=<' | \
grep 'from=' | \
sed "s#from=<##g" | \
sed "s#>##g" | \
grep '@' | \
uniq -u
echo -e "\n"
done

exit 0
