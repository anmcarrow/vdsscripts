#!/bin/bash -eu

declare -r EZMSRC="https://github.com/shevabam/ezservermonitor-web.git"
declare -r EZMPATH="$1"

#read -p "Path to install: " EZMPATH
#export EZMPATH

do_deploy() {
git clone ${EZMSRC} ${EZMPATH}
}

do_secure() {
read -p "Adminname: " EZMADM
read -p "Password: " EZMPW 
htpasswd -cb ${EZMPATH}/.htpasswd ${EZMADM} ${EZMPW} 
echo -e "AuthType Basic \nAuthName 'Private zone' \nAuthUserFile  ${EZMPATH}/.htpasswd \nrequire valid-user" \
> ${EZMPATH}/.htaccess
}

do_deploy
do_secure

printf 'Done!'

exit 0
