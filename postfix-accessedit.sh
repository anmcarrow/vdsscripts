#!/bin/bash

YELLOW='\033[0;33m'
BLUE='\033[0;34m'
BW='\033[0m'

echo -e "${YELLOW} Start editing... ${BW}"
nano /etc/postfix/access

echo -e ""
echo -e "${YELLOW} Compiling... ${BW}"
postmap /etc/postfix/access

echo -e ""
echo -e "${YELLOW} Applying... ${BW}"
echo -e ""
/etc/init.d/postfix restart

echo -e ""
echo -e "${BLUE} Done! ${BW}"

