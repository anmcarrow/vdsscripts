#!/bin/bash -eu

declare BODY='Hello Dear! And welcome to the game!'

local FROM=${1}
local TO=${2}

echo $BODY | mail \
-a "From: ${FROM}" \
-a "MIME-Version: 1.0" \
-a "Content-Type: text/html" \
-a "Return-Path: <${FROM}>" \
-s "Welcome to the game!" \
${TO}
