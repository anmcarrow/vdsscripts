#!/bin/sh -eu

filter () {
	awk '
	BEGIN{BODY=0}
	{
		if ($1 == "create_time:") {$1 = ""; TIME = $0};
		if ($1 == "sender:") {SENDER = $2};
		if (/\*\*\* HEADER EXTRACTED/) {BODY = 0};
		if (BODY == 1) {print};
		if (/\*\*\* MESSAGE CONTENTS/) {BODY = 1; print "From",SENDER,TIME};
	}'
}

if tty > /dev/null; then
	MBX=$(mktemp)
else
	MBX=/dev/stdout
fi

for QUEUE in $@; do
	if [ -e $QUEUE ]; then
		echo "From 1@1.1 Sun Feb 29 00:00:00 1980" >> $MBX;
		cat $QUEUE >> $MBX;
	else
		postcat -q $QUEUE | filter >> $MBX
	fi

done

if tty > /dev/null; then
	mutt -f $MBX || true
	rm -f $MBX
fi
