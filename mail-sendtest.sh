#!/bin/bash

# Just a simple script, who sending a little test mail on some address

ADDR="$1"
# MAILER="mail"
MAILER="mutt"
SUBJ="Just a test mail, sir"
TEXT="Lorem ipsum, dolor sit amet."

echo "${TEXT}" | ${MAILER} -s "${SUBJ}" ${ADDR}

exit 0
