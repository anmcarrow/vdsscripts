#!/bin/bash

mailq | sed '/^[A-F0-9]/ {s,^\([A-F0-9]*\).*$,\1,; p}; d' | postsuper -d -

exit 0
