#!/bin/bash


EZMSOURCE="https://raw.githubusercontent.com/shevabam/ezservermonitor-sh/master/eZServerMonitor.sh"

declare -r EZMDEST="$(mktemp -p /var/tmp)"

ezmmain() {
wget -qO- ${EZMSOURCE} > ${EZMDEST}
chmod +x ${EZMDEST}
${EZMDEST} -a
rm ${EZMDEST}
}

ezmmain

exit 0
