#!/bin/bash -eu

# daily databases backup
# requirements: mysqldump, date, gzip

# script use credentials from ~/.my.cnf

LANG=C
OWNER="root"
CREDS="${HOME}/.my.cnf"

DATE=`/bin/date`
DAY=`/bin/date +%F`
TIME="$(date +%H-%M)"
ROTATEDAY="$(date -d '30 days ago' +%F)"
BACKUPDIR="/home/mysqlbackup"
MYHOST="localhost"

OLDFILES="$(ls -1 ${BACKUPDIR}/${HOSTNAME}-*-${ROTATEDAY}*.sql.gz 2>/dev/null | wc -l)"
BACKUPFILE="${BACKUPDIR}/${DAY}/${HOSTNAME}-dbdump-$DAY.gz"

mysqllist() {

mysqlshow | \
grep -vE "Databases|performance_schema|mysql|information_schema|\+"  \
| tr -d '|'

}

# Make database backup
mkdir -p $BACKUPDIR

# Dumping bases

for DB in $(mysqllist)
do
    mysqldump --defaults-extra-file=$CREDS --add-drop-table -R $DB | gzip -c --best > \
    ${BACKUPDIR}/${HOSTNAME}-${DB}-${DAY}-${TIME}.sql.gz && 
    logger "DB ${DB} backup is done at ${DATE}"
done

chmod -R a+r ${BACKUPDIR}

# Cleaning old backups


if [[ ${OLDFILES} -gt "0" ]] 
    then
        rm ${BACKUPDIR}/${HOSTNAME}-${DB}-${ROTATEDAY}*.sql.gz 2>/dev/null
fi

exit 0
